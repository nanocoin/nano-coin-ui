//var sslRedirect = require('heroku-ssl-redirect');
const express = require('express');
const app = express();

//app.use(sslRedirect());

app.use(express.static(__dirname + '/dist'));
// Heroku port
app.listen(process.env.PORT || 8089);
app.get('/*', function(req, res) {
  res.sendfile(__dirname + '/dist/index.html')
});
