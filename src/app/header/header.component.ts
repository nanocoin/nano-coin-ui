import {Component} from '@angular/core';
import {AuthorizationService} from '../common/authorization.service';
import {Router} from '@angular/router';
import {HttpService} from '../common/http-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent {
  isAdmin = false;
  username = '';
  constructor(private authorizationService: AuthorizationService,
              private router: Router,
              private httpService: HttpService) {
    if (this.authorizationService.isAdmin()) {
      this.isAdmin = true;
      router.navigate(['/admin']);
    } else {
      this.username = authorizationService.getUserDetails().username;
    }
  }

  logout() {
    this.httpService.logout();
  }

}
