
import {Component, Input, OnInit} from '@angular/core';
import {UserDeposit} from '../model/deposit';
import {DepositCalculator} from '../model/deposit-calculator';
import {DepositComponent} from '../deposit.component';
import {CommonUtilsService} from 'app/common/common-utils-service';
import {Message} from '../../common/model/message.model';

@Component({
  selector: 'app-create-deposit',
  templateUrl: './create-deposit.component.html'
})
export class CreateDepositComponent implements OnInit {

  @Input('depositComponent') depositComponent: DepositComponent;

  customDepositPlanId = -1;

  confirmPrefix = 'confirm_';
  cancelPrefix = 'cancel_';

  waitingDeposits: UserDeposit[] = [];
  waitingForPaymentDeposits: UserDeposit[] = [];
  paymentMethods: string[] = [];
  selectedPaymentMethod = '';

  maxCustomDepositProfit = 0;
  minCustomDepositProfit = 0;
  months = '';
  sum = 0;
  depositName = '';
  selectedDepositPlanId = '';

  maxDepositSum = 500000;

  ngOnInit() {
    this.loadWaitingForPaymentDeposits();
    this.loadWaitingDeposits();
    this.loadPaymentMethods();
  }

  calculateCustomDepositTotalSumRange() {
    if (this.sum != null) {
      if (this.sum > this.maxDepositSum) {
        this.sum = this.maxDepositSum;
      }
      this.depositComponent.loading = true;
      const deposit = new UserDeposit();
      deposit.sum = this.sum;
      deposit.depositPlanId = this.customDepositPlanId;
      this.depositComponent.depositSerivce.calculateIncomeForSum(deposit)
        .subscribe(response => {
          const depositCalculator = (response as DepositCalculator);
          if (depositCalculator.message === '') {
            this.maxCustomDepositProfit = depositCalculator.maxSum;
            this.minCustomDepositProfit = depositCalculator.minSum;
            this.months = depositCalculator.months;
          } else {
            this.depositComponent.localNotificationService.notifyByPrefix('DEPOSIT.', depositCalculator.message);
            this.maxCustomDepositProfit = 0;
            this.minCustomDepositProfit = 0;
            this.months = '';
          }
          this.depositComponent.loading = false;
        });
    } else {
      this.maxCustomDepositProfit = 0;
      this.minCustomDepositProfit = 0;
      this.months = '';
    }
  }

  loadWaitingDeposits() {
    this.depositComponent.loading = true;
    this.depositComponent.depositSerivce.getUserWaitingDeposits()
      .subscribe(response => {
        this.waitingDeposits = (response as UserDeposit[]);
        this.depositComponent.loading = false;
      });
  }

  loadWaitingForPaymentDeposits() {
    this.depositComponent.loading = true;
    this.depositComponent.depositSerivce.getUserWaitingForPaymentDeposits()
      .subscribe(response => {
        this.waitingForPaymentDeposits = [];
        (response as UserDeposit[]).forEach(deposit => {
          const userDeposit = deposit;
          this.waitingForPaymentDeposits.push(userDeposit);
          this.depositComponent.translateService.get('DEPOSIT.PAY_BUTTON')
            .subscribe(translation => {
              this.depositComponent.paymentForm = userDeposit.paymentForm.replace('PAY_BUTTON', translation);
              this.depositComponent.loading = false;
            });
        });
      });
  }

  loadPaymentMethods() {
    this.depositComponent.loading = true;
    this.depositComponent.paymentService.getAvailablePaymentMethods()
      .subscribe(response => {
        this.paymentMethods = (response as string[]);
        this.depositComponent.loading = false;
      });
  }

  createCustomDeposit() {
    this.depositComponent.loading = true;
    const userDeposit = new UserDeposit();
    if (this.sum > this.maxDepositSum) {
      this.sum = this.maxDepositSum;
    }
    userDeposit.sum = this.sum;
    userDeposit.depositPlanId = this.customDepositPlanId;
    this.depositComponent.depositSerivce.createDeposit(userDeposit)
      .subscribe(response => {
        const createdDepoist = (response as UserDeposit);
        if (createdDepoist.message !== '') {
          this.depositComponent.localNotificationService.notifyByPrefix('DEPOSIT.', createdDepoist.message);
        } else {
          this.waitingDeposits.push(createdDepoist);
          this.sum = 0;
        }
        this.depositComponent.loading = false;
      });
  }

  confirmDeposit(event) {
    const depositId = CommonUtilsService.getIdByEvent(event).replace(this.confirmPrefix, '');
    if (this.selectedPaymentMethod === '') {
      this.depositComponent.localNotificationService.notifyError('DEPOSIT.SELECT_PAYMENT_METHOD');
    } else {
      this.depositComponent.loading = true;
      const userDeposit = new UserDeposit();
      userDeposit.depositId = depositId;
      userDeposit.depositName = this.depositName;
      userDeposit.paymentMethod = this.selectedPaymentMethod;
      userDeposit.depositPlanId = this.customDepositPlanId;
      this.depositComponent.depositSerivce.confirmDeposit(userDeposit)
        .subscribe(response => {
          const confirmedDeposit = (response as UserDeposit);
          this.waitingDeposits = [];
          this.waitingForPaymentDeposits = [confirmedDeposit];
          this.depositComponent.translateService.get('DEPOSIT.PAY_BUTTON')
            .subscribe(translation => {
              this.depositComponent.paymentForm = confirmedDeposit.paymentForm.replace('PAY_BUTTON', translation);
              this.depositComponent.loading = false;
            });
        });
    }
  }

  cancelDeposit(event) {
    this.depositComponent.loading = true;
    const depositId = CommonUtilsService.getIdByEvent(event).replace(this.cancelPrefix, '');
    this.depositComponent.depositSerivce.cancelDeposit(depositId)
      .subscribe(response => {
        this.waitingDeposits = [];
        this.waitingForPaymentDeposits = [];
        this.depositComponent.paymentForm = '';
        this.depositComponent.localNotificationService.notifyError('DEPOSIT.' + (response as Message).text);
        this.depositComponent.loading = false;
        this.maxCustomDepositProfit = 0;
        this.minCustomDepositProfit = 0;
        this.months = '';
      });
  }

}
