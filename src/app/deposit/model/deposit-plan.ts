
export class DepositPlan {
  depositCategoryName: string;
  depositPlanId: string;
  depositPlanName: string;
  minSum: string;
  binaryBonusPercents: string;
  months: string;
  monthlyIncome: string;
}
