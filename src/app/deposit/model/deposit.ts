
export class UserDeposit {
  depositId: string;
  depositPlanId: number;
  depositPlanName: string;
  depositName: string;
  message: string;
  sum: number;
  currency: string;
  startDate: string;
  endDate: string;
  moneyTotal: string;
  moneyTotalMax: string;
  monthlyIncome: string;
  maxMonthlyIncome: string;
  moneyReceived: string;
  moneyLeft: string;
  depositStatus: string;
  paymentMethod: string;
  paymentForm: string;
}
