import {Component, ElementRef, ViewChild} from '@angular/core';
import {DepositService} from './deposit.service';
import {UserDeposit} from './model/deposit';
import {PaymentService} from '../payment/payment.service';
import {TranslateService} from '@ngx-translate/core';
import {LocalNotificationService} from '../notification/local.notification.service';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html'
})
export class DepositComponent {

  loading = false;

  activeDeposits: UserDeposit[] = [];
  closedDeposits: UserDeposit[] = [];

  showActiveDeposits = true;
  showClosedDeposits = false;
  createDeposit = false;

  @ViewChild('paymentFormContainer') paymentFormContainer: ElementRef;
  paymentForm = '';

  constructor(public depositSerivce: DepositService,
              public paymentService: PaymentService,
              public translateService: TranslateService,
              public localNotificationService: LocalNotificationService) {
    this.showActiveDepositsAction();
  }

  showActiveDepositsAction() {
    this.createDeposit = false;
    this.showClosedDeposits = false;
    this.showActiveDeposits = true;
    this.loadActiveDeposits();
  }

  showClosedDepositsAction() {
    this.createDeposit = false;
    this.showActiveDeposits = false;
    this.showClosedDeposits = true;
    this.loadClosedDeposits();
  }

  createDepositAction() {
    this.showActiveDeposits = false;
    this.showClosedDeposits = false;
    this.createDeposit = true;
  }

  loadActiveDeposits() {
    this.loading = true;
    this.depositSerivce.getUserActiveDeposits()
        .subscribe(response => {
          this.activeDeposits = (response as UserDeposit[]);
          this.loading = false;
        });
  }

  loadClosedDeposits() {
    this.loading = true;
    this.depositSerivce.getUserClosedDeposits()
        .subscribe(response => {
          this.closedDeposits = (response as UserDeposit[])
          this.loading = false;
        });
  }
}


