import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Url} from '../app.urls';
import {Observable} from 'rxjs/Observable';
import {UserDeposit} from 'app/deposit/model/deposit';

@Injectable()
export class DepositService {

  constructor(private httpService: HttpService) {
  }

  getUserActiveDeposits(): Observable<Object> {
    return this.httpService.getObservable(Url.get_all_active_deposits);
  }

  getUserClosedDeposits(): Observable<Object> {
    return this.httpService.getObservable(Url.get_all_closed_deposits)
  }

  getUserWaitingDeposits(): Observable<Object> {
    return this.httpService.getObservable(Url.get_all_waiting_deposits);
  }

  getUserWaitingForPaymentDeposits(): Observable<Object> {
    return this.httpService.getObservable(Url.get_all_waiting_for_payment);
  }

  calculateIncomeForSum(userDeposit: UserDeposit): Observable<Object> {
    return this.httpService.postObservable(Url.calculate_income_for_sum, JSON.stringify(userDeposit));
  }

  createDeposit(userDeposit: UserDeposit): Observable<Object> {
    return this.httpService.postObservable(Url.create_deposit, JSON.stringify(userDeposit));
  }

  confirmDeposit(userDeposit: UserDeposit): Observable<Object> {
    return this.httpService.postObservable(Url.confirm_deposit, JSON.stringify(userDeposit));
  }

  cancelDeposit(depositId: string): Observable<Object> {
    const userDeposit = new UserDeposit();
    userDeposit.depositId = depositId;
    return this.httpService.postObservable(Url.cancel_deposit, JSON.stringify(userDeposit));
  }

  getDaysDiffStr(givenDateStr: string) {
    return Math.ceil((new Date(givenDateStr).getTime() - new Date().getTime()) / (1000 * 3600 * 24));
  }

}
