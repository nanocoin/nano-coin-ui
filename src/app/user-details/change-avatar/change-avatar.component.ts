import {Component, Input} from '@angular/core';
import {UserDetailsComponent} from '../user-details.component';
import {Url} from '../../app.urls';

@Component({
  selector: 'app-change-avatar',
  templateUrl: './change-avatar.component.html'
})
export class ChangeAvatarComponent {

  @Input('userDetailsComponent') userDetailsComponent: UserDetailsComponent;

  loadAvatar(event) {
    const fileList = event.target.files;
    this.userDetailsComponent.loadFile(event, Url.save_avatar, 'USER_DETAILS.AVATAR_CHANGED', 'USER_DETAILS.AVATAR_CHANGE_ERROR');
  }
}
