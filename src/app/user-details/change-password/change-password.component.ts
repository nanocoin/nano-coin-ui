
import {Component, Input} from '@angular/core';
import {LocalNotificationService} from 'app/notification/local.notification.service';
import {Message} from '../../common/model/message.model';
import {UserDetailsComponent} from '../user-details.component';
import {UserDetailsService} from '../user-details.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent {

  @Input('userDetailsComponent') userDetailsComponent: UserDetailsComponent;

  oldPassword = '';
  confirmOldPassword = '';
  newPassword = '';

  constructor(private userDetailsService: UserDetailsService,
              private localNotificationService: LocalNotificationService) {}

  changePassword() {
    if (this.oldPassword !== this.confirmOldPassword) {
      this.localNotificationService.notifyError('USER_DETAILS.PASSWORDS_ARE_DIFFERENT');
    } else if (this.newPassword.length < 6 || this.newPassword.length > 20) {
      this.localNotificationService.notifyError('USER_DETAILS.INVALID_PASSWORD_LENGTH');
    } else {
      this.userDetailsComponent.loading = true;
      this.userDetailsService.changePassword(this.oldPassword, this.newPassword)
        .subscribe(response => {
          this.localNotificationService.notifyByPrefix('USER_DETAILS.', (response as Message).text);
          this.oldPassword = '';
          this.confirmOldPassword = '';
          this.newPassword = '';
          this.userDetailsComponent.loading = false;
        });
    }
  }

}
