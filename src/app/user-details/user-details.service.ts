
import {Injectable} from '@angular/core';
import {UserInfo} from '../main/model/user-info';
import {Observable} from 'rxjs/Observable';
import {HttpService} from '../common/http-service';
import {Url} from '../app.urls';

@Injectable()
export class UserDetailsService {

  constructor(private httpService: HttpService) {  }

  updateUserInfo(userInfo: UserInfo): Observable<Object> {
    return this.httpService.postObservable(Url.update_user_info, JSON.stringify(userInfo));
  }

  changePassword(oldPassword: string, newPassword: string): Observable<Object> {
    const userInfo = new UserInfo();
    userInfo.oldPassword = oldPassword;
    userInfo.newPassword = newPassword;
    return this.httpService.postObservable(Url.change_password, JSON.stringify(userInfo));
  }
}
