import {Component, Input, OnInit} from '@angular/core';
import {UserDetailsComponent} from '../user-details.component';
import {UserInfo} from '../../main/model/user-info';
import {IMyDpOptions} from 'mydatepicker';
import {AuthorizationService} from '../../common/authorization.service';
import {TranslateService} from '@ngx-translate/core';
import {LocalNotificationService} from '../../notification/local.notification.service';
import {UserDetailsService} from '../user-details.service';
import {Error400} from '../../common/model/error.model';
import {Url} from '../../app.urls';
import {CountryService} from '../../country/country.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html'
})
export class AccountSettingsComponent implements OnInit {

  @Input('userDetailsComponent') userDetailsComponent: UserDetailsComponent;

  userInfo: UserInfo;
  genders = [];
  gender = '';

  birthdateModel: any;
  datePickerOptions: IMyDpOptions = {dateFormat: 'yyyy.mm.dd'};

  constructor(private authorizationService: AuthorizationService,
              private translateService: TranslateService,
              private localNotificationService: LocalNotificationService,
              private userDetailsService: UserDetailsService,
              public countryService: CountryService) {}

  ngOnInit(): void {
    this.userInfo = this.authorizationService.getUserDetails();
    this.setBirthdate(this.userInfo.birthdayDate);
    this.setGender();
  }

  setBirthdate(birthDate: string) {
    if (birthDate !== '') {
      const parsedBirthdate = birthDate.split('-');
      this.birthdateModel = {
        date: {
          year: parsedBirthdate[0],
          month: parsedBirthdate[1],
          day: parsedBirthdate[2]
        }
      };
    }
  }

  setGender() {
    this.translateService.get('USER_DETAILS.GENDER_MAN')
      .subscribe(genderMan => {
        this.translateService.get('USER_DETAILS.GENDER_WOMAN')
          .subscribe(genderWoman => {
            this.genders = [genderMan, genderWoman];
            if ('' + this.userInfo.man === 'true') {
              this.gender = genderMan;
            } else {
              this.gender = genderWoman;
            }
          })
      });
  }

  parseBirthdateString() {
    return this.birthdateModel['date']['year'] + '-' + this.birthdateModel['date']['month'] + '-' + this.birthdateModel['date']['day'];
  }

  save() {
    let date = null;
    try {
      date = this.parseBirthdateString();
    } catch (Error) {
      this.localNotificationService.notifyError('USER_DETAILS.WRONG_DATE_FORMAT');
      return;
    }
    this.userInfo.birthdayDate = date;
    this.translateService.get('USER_DETAILS.GENDER_MAN')
      .subscribe(genderMan => {
        this.userInfo.man = this.gender === genderMan;
        this.userDetailsService.updateUserInfo(this.userInfo)
          .subscribe(response => {
            if (response instanceof Error400) {
              response.errors.forEach(error => {
                this.localNotificationService.notifyError('USER_DETAILS.' + error);
              })
            } else {
              this.localNotificationService.notifyCreate('USER_DETAILS.CHANGES_SAVED');
              localStorage.setItem(AuthorizationService.authTokenKey, response['storageToken']);
            }
          });
      });
  }

  loadPassportScan(event) {
    this.userDetailsComponent.loadFile(event, Url.save_passport_scan, 'USER_DETAILS.PASSPORT_SCAN_UPLOADED', 'USER_DETAILS.PASSPORT_SCAN_UPLOAD_FAILED');
  }
}
