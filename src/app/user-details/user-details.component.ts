import {Component, ViewChild} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {LocalNotificationService} from 'app/notification/local.notification.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html'
})
export class UserDetailsComponent {

  loading = false;

  accountSettingsState: Boolean = true;
  avatarSettingsState: Boolean = false;
  changePasswordState: Boolean = false;

  @ViewChild('fileInput') fileInput;

  constructor(private httpService: HttpService,
              private ng2ImgMax: Ng2ImgMaxService,
              private localNotificationService: LocalNotificationService) {  }


  showAccountSettings() {
    this.accountSettingsState = true;
    this.avatarSettingsState = false;
    this.changePasswordState = false;
  }

  showAvatarSettings() {
    this.accountSettingsState = false;
    this.avatarSettingsState = true;
    this.changePasswordState = false;
  }

  showChangePassword() {
    this.accountSettingsState = false;
    this.avatarSettingsState = false;
    this.changePasswordState = true;
  }

  loadFile(event, url: string, okMessageCode: string, errorMessageCode: string) {
    this.loading = true;
    const fileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      this.ng2ImgMax.resizeImage(file, 1000, 1000).subscribe(
          resizedFile => {
            const formData = new FormData();
            formData.append('file', new File([resizedFile], resizedFile.name));
            this.httpService.postImage(url, formData)
                .subscribe(response => {
                  this.localNotificationService.notifyCreate(okMessageCode);
                  this.loading = false;
                });
          },
          error => {
            this.localNotificationService.notifyError(errorMessageCode);
            this.loading = false;
          });
    }
  }

}
