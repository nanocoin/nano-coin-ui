import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {RegistrationService} from './registration.service';
import {ActivatedRoute} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {LocalNotificationService} from 'app/notification/local.notification.service';
import {Error400} from '../common/model/error.model';
import {Token} from '../common/model/token.model';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit {

  @Input('loginComponent') loginComponent: LoginComponent;

  public loading = false;

  showReferralId = false;

  public registrationForm = this.fb.group({
      left: [''],
      invitedUserId: [],
      parentReferralId: [''],
      email: [''],
      username: [''],
      password: ['']
    }
  )

  constructor(private registrationService: RegistrationService,
              private fb: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private localNotificationService: LocalNotificationService) {  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.paramMap.get('parentReferralId') != null) {
      this.loginComponent.openRegistrationForm();
      const userId = this.activatedRoute.snapshot.paramMap.get('parentReferralId').substring(0, this.findIdEndIndx());
      this.registrationForm.controls['invitedUserId'].patchValue(userId);
      const parentReferralId = this.activatedRoute.snapshot.paramMap.get('parentReferralId').substring(this.findIdEndIndx() + 1);
      this.registrationForm.controls['parentReferralId'].patchValue(parentReferralId);
      const left = this.activatedRoute.snapshot.paramMap.get('parentReferralId').substring(this.findIdEndIndx(), this.findIdEndIndx() + 1);
      if (left === 'l') {
        this.registrationForm.controls['left'].patchValue('true');
      } else {
        this.registrationForm.controls['left'].patchValue('false');
      }
      this.showReferralId = true;
    }
  }

  findIdEndIndx() {
    let indx = 0;
    while (!Number.isNaN(+this.activatedRoute.snapshot.paramMap.get('parentReferralId')[indx])) {
      indx++;
    }
    return indx;
  }

  register() {
    this.loading = true;
    this.registrationService
        .register(this.registrationForm.value)
        .subscribe(response => {
          if (response instanceof Error400) {
            response.errors.forEach(error => {
              this.localNotificationService.notifyError('REGISTER.FORM.' + error);
            })
          } else {
            this.localNotificationService.notifyByPrefix('REGISTER.FORM.', (response as Token).message);
          }
          this.loading = false;
        });
  }
}
