
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';

@Injectable()
export class RegistrationService {

  constructor(private httpService: HttpService) {  }

  register(loginInfoJson: JSON): Observable<Object> {
    return this.httpService.postObservable(Url.create_user, JSON.stringify(loginInfoJson));
  }

}
