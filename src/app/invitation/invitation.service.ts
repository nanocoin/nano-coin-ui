
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';
import {Invitation} from '../main/model/invitation';

@Injectable()
export class InvitationService {

  constructor(private httpService: HttpService) {  }

  sendInvitationLink(invitation: Invitation): Observable<Object> {
    return this.httpService
               .postObservable(Url.invitation_link, JSON.stringify(invitation));
  }
}
