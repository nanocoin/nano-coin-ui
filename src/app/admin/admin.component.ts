import {Component} from '@angular/core';
import {AdminService} from './admin.service';
import {UserInfo} from '../main/model/user-info';
import {CommonUtilsService} from '../common/common-utils-service';
import {AuthorizationService} from '../common/authorization.service';
import {Withdrawal} from '../account/model/withdrawal';
import {News} from '../news/model/news.model';
import {NewsService} from '../news/news.service';
import {TranslateService} from '@ngx-translate/core';
import {LocalNotificationService} from 'app/notification/local.notification.service';
import {Error400} from '../common/model/error.model';
import {MatDialog} from '@angular/material/dialog';
import {ImageExplorerComponent} from '../image-explorer/image-explorer.component';
import {AdminSettings} from './model/admin-settings';
import {PaymentService} from '../payment/payment.service';
import {UserDetailsService} from 'app/user-details/user-details.service';
import {Message} from '../common/model/message.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'})
export class AdminComponent {

  public loading = false;

  users: UserInfo[] = [];
  newsList: News[] = [];

  activeWithdrawals: Withdrawal[] = [];

  toggleLockPrefix = 'toggleLock_';
  closeWithdrawalPrefix = 'closeWithdrawal_';
  cancelWithdrawalPrefix = 'cancelWithdrawal_';
  newsIdPrefix = 'newsId_';

  news = new News();

  adminSettings = new AdminSettings();

  showWithdrawals = false;
  showUsers = false;
  showNews = false;
  showSettings = false;

  paymentMethods: string[] = [];

  oldPassword = '';
  confirmOldPassword = '';
  newPassword = '';

  searchUsername = '';

  usersPage = 1;

  constructor(private adminService: AdminService,
              private authorizationService: AuthorizationService,
              private newsService: NewsService,
              private translate: TranslateService,
              private localNotificationService: LocalNotificationService,
              private matDialog: MatDialog,
              private paymentService: PaymentService,
              private userDetailsService: UserDetailsService) {
    this.showWithdrawalsAction();
    this.loadPaymentMethods();
  }

  showWithdrawalsAction() {
    this.getActiveWithdrawals();
    this.showUsers = false;
    this.showNews = false;
    this.showWithdrawals = true;
    this.showSettings = false;
  }

  showUsersAction() {
    this.loadUsers();
    this.showNews = false;
    this.showWithdrawals = false;
    this.showUsers = true;
    this.showSettings = false;
  }

  showNewsAction() {
    this.loadNews();
    this.showUsers = false;
    this.showWithdrawals = false;
    this.showNews = true;
    this.showSettings = false;
  }

  showSettingsAction() {
    this.loadAdminSettings();
    this.showUsers = false;
    this.showWithdrawals = false;
    this.showNews = false;
    this.showSettings = true;
  }

  loadPaymentMethods() {
    this.paymentService.getAvailableWithdrawPaymentMethods()
        .subscribe(response => this.paymentMethods = response as string[]);
  }

  loadNews() {
    this.newsService.getNews()
        .subscribe(response => this.newsList = response as News[]);
  }

  closeWithdrawalRequest(event) {
    const withdrawalId = CommonUtilsService.getIdByEvent(event).replace(this.closeWithdrawalPrefix, '');
    const withdrawal = this.activeWithdrawals.find(it => it.withdrawalId === withdrawalId)
    this.adminService.closeWithdrawal(withdrawal)
        .subscribe(response => {
          const message = (response as Message).text;
          if (message === 'APPROVED') {
            this.localNotificationService.notifyCreate('ADMIN.WITHDRAWAL.' + message);
          } else {
            this.localNotificationService.notifyError('ADMIN.WITHDRAWAL.' + message);
          }
          withdrawal.password = '';
          this.getActiveWithdrawals();
        });
  }

  cancelWithdrawalRequest(event) {
    const withdrawalId = CommonUtilsService.getIdByEvent(event).replace(this.cancelWithdrawalPrefix, '');
    const withdrawal = new Withdrawal();
    withdrawal.withdrawalId = withdrawalId;
    this.adminService.cancelWithdrawal(withdrawal)
        .subscribe(response => {
          this.localNotificationService.notifyError('ADMIN.WITHDRAWAL.CANCELLED');
          this.getActiveWithdrawals();
        });
  }

  loadUsers() {
    this.adminService.getUsers()
        .subscribe(response => this.users = response as UserInfo[]);
  }

  showImage(imagePath: string) {
    this.matDialog.open(ImageExplorerComponent,
      {data: { imagePath: imagePath}});
  }

  toggleLockAction(event) {
    this.loading = true;
    const userId = CommonUtilsService.getIdByEvent(event).replace(this.toggleLockPrefix, '');
    this.adminService.toggleUserLock(userId)
        .subscribe(response => {
          const user = this.users.find(u => u.userId === userId);
          user.locked = !user.locked;
          this.loading = false;
        });
  }

  getActiveWithdrawals() {
    this.adminService
        .getActiveWithdrawals()
        .subscribe(response => this.activeWithdrawals = response as Withdrawal[]);
  }

  postNews() {
   this.adminService.postNews(this.news)
      .subscribe(response => {
          if (response instanceof Error400) {
            response.errors.forEach(error => {
              this.localNotificationService.notifyError('ADMIN.NEWS.' + error);
            })
          } else if ((response as Message).text !== '') {
            this.localNotificationService.notifyByPrefix('ADMIN.NEWS.', (response as Message).text);
          } else {
            this.localNotificationService.notifyCreate('ADMIN.NEWS.ADDED');
            this.loadNews();
          }
        });
  }

  deleteNews(event) {
    const newsId = CommonUtilsService.getIdByEvent(event).replace(this.newsIdPrefix, '');
    this.adminService.deleteNews(newsId)
        .subscribe(response => {
          this.localNotificationService.notifyError('ADMIN.NEWS.DELETED');
          this.loadNews();
      });
  }

  loadAdminSettings() {
    this.adminService.getAdminSettings()
        .subscribe(response => this.adminSettings = response as AdminSettings);
  }

  findUser() {
    this.users.forEach((it, indx) => {
      if (it.username === this.searchUsername) {
        this.usersPage = indx + 1;
      }
    });
  }

  saveSettings() {
    if (this.adminSettings.dailyDepositPercents == null ||
        this.adminSettings.dailyDepositPercents < 0.6 || this.adminSettings.dailyDepositPercents > 1) {
      this.localNotificationService.notifyError('ADMIN.SETTINGS.WRONG_DEPOSIT_PERCENT');
    } else {
      this.adminService.saveAdminSettings(this.adminSettings)
          .subscribe(response => {
            if ((response as Message).text !== '') {
              this.localNotificationService.notifyError('ADMIN.SETTINGS.WRONG_DEPOSIT_PERCENT');
            } else {
              this.localNotificationService.notifyCreate('ADMIN.SETTINGS.SUCCESFULLY_SAVED');
            }
          });
    }
  }

  changePassword() {
    this.loading = true;
    if (this.oldPassword !== this.confirmOldPassword) {
      this.localNotificationService.notifyError('USER_DETAILS.PASSWORDS_ARE_DIFFERENT');
      this.loading = false;
    } else if (this.newPassword.length < 6 || this.newPassword.length > 20) {
      this.localNotificationService.notifyError('USER_DETAILS.INVALID_PASSWORD_LENGTH');
      this.loading = false;
    } else {
      this.userDetailsService.changePassword(this.oldPassword, this.newPassword)
        .subscribe(response => {
          this.localNotificationService.notifyByPrefix('USER_DETAILS.', (response as Message).text);
          this.oldPassword = '';
          this.confirmOldPassword = '';
          this.newPassword = '';
          this.loading = false;
        });
    }
  }

  logout() {
    this.authorizationService.logout();
  }
}
