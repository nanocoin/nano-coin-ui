
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';
import {UserInfo} from '../main/model/user-info';
import {News} from '../news/model/news.model';
import {Withdrawal} from '../account/model/withdrawal';
import {AdminSettings} from './model/admin-settings';

@Injectable()
export class AdminService {

  constructor(private httpService: HttpService) {
  }

  getUsers(): Observable<Object> {
    return this.httpService.getObservable(Url.get_users);
  }

  toggleUserLock(userId: string): Observable<Object> {
    const userInfo = new UserInfo();
    userInfo.userId = userId;
    return this.httpService.postObservable(Url.toggle_lock_user, JSON.stringify(userInfo));
  }

  getActiveWithdrawals(): Observable<Object> {
    return this.httpService.getObservable(Url.get_all_active_withdrawals);
  }
  closeWithdrawal(withdrawal: Withdrawal): Observable<Object> {
    return this.httpService.postObservable(Url.close_withdrawal, JSON.stringify(withdrawal));
  }
  cancelWithdrawal(withdrawal: Withdrawal): Observable<Object> {
    return this.httpService.postObservable(Url.cancel_withdrawal, JSON.stringify(withdrawal));
  }

  getAdminSettings(): Observable<Object> {
    return this.httpService.getObservable(Url.get_admin_settings);
  }

  saveAdminSettings(adminSettings: AdminSettings): Observable<Object> {
    return this.httpService.postObservable(Url.save_admin_settings, JSON.stringify(adminSettings));
  }

  getNews(): Observable<Object> {
    return this.httpService.getObservable(Url.news);
  }
  postNews(news: News): Observable<Object> {
    return this.httpService.postObservable(Url.news, JSON.stringify(news));
  }
  deleteNews(newsId: string): Observable<Object> {
    const news = new News();
    news.id = newsId;
    return this.httpService.deleteObservable(Url.news, news.id);
  }
}
