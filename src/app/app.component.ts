import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

  style: object = {};
  params: object = {};
  width = 100;
  height = 100;

  options = {
    position: ['bottom', 'right'],
    timeOut: 4000
  }

  constructor() {
  }

  ngOnInit() {
    this.style = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
    };

    this.params = {
      particles: {
        number: {
          value: 100,
        },
        color: {
          value: ['#858585']
        },
        line_linked: {
          color: '#858585',
          opacity: 1
        }
      }
    };
  }

}
