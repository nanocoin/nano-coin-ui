
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NotificationsService} from 'angular2-notifications';

@Injectable()
export class LocalNotificationService {

  ERROR_PREFIX = 'WRONG';

  constructor(private translateService: TranslateService,
              private notificationsService: NotificationsService) { }

  notifyError(messageCode: string) {
    this.translateService.get(messageCode)
      .subscribe(translation => {
        this.notificationsService.error(translation);
      });
  }

  notifyCreate(messageCode: string) {
    this.translateService.get(messageCode)
      .subscribe(translation => {
        this.notificationsService.create(translation);
      });
  }

  notifyByPrefix(messageCodePrefix: string, messageCode: string) {
    this.translateService.get(messageCodePrefix + messageCode)
      .subscribe(translation => {
        if (messageCode.startsWith(this.ERROR_PREFIX)) {
          this.notificationsService.error(translation);
        } else {
          this.notificationsService.create(translation);
        }
      });
  }
}
