import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthorizationService} from './common/authorization.service';
import {AdminComponent} from './admin/admin.component';
import {DepositComponent} from './deposit/deposit.component';
import {TeamComponent} from './team/team.component';
import {AccountComponent} from './account/account.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {NewsComponent} from './news/news.component';
import {AboutCompanyComponent} from './about-company/about-company.component';
import {ContactsComponent} from './contacts/contacts';

export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'login/:parentReferralId', component: LoginComponent },
  { path: 'news', component: NewsComponent, canActivate: [AuthorizationService] },
  { path: 'deposit', component: DepositComponent, canActivate: [AuthorizationService] },
  { path: 'team', component: TeamComponent, canActivate: [AuthorizationService] },
  { path: 'account', component: AccountComponent, canActivate: [AuthorizationService] },
  { path: 'user-details', component: UserDetailsComponent, canActivate: [AuthorizationService] },
  { path: 'about-company', component: AboutCompanyComponent, canActivate: [AuthorizationService] },
  { path: 'contacts', component: ContactsComponent, canActivate: [AuthorizationService] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthorizationService] },
  { path: '**', component: NewsComponent, canActivate: [AuthorizationService] }
];
