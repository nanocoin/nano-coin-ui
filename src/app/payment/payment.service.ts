
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';

import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';

@Injectable()
export class PaymentService {

  constructor(private httpService: HttpService) { }

  getAvailablePaymentMethods(): Observable<Object> {
    return this.httpService.getObservable(Url.get_available_payment_methods);
  }

  getAvailableWithdrawPaymentMethods(): Observable<Object> {
    return this.httpService.getObservable(Url.get_available_withdraw_payment_methods);
  }
}
