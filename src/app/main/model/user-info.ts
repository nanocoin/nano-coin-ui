
export class UserInfo {
  userId = '';
  email = '';
  username = '';
  name = '';
  surname = '';
  contactNumber = '';
  contactNumber2 = '';
  man = true;
  birthdayDate = '';
  passportId = '';
  address = '';
  city = '';
  country = '';
  referralId = '';
  parentEmail = '';
  roleName = '';
  active = '';
  locked = false;
  imagePath = '';
  passportScanPath = '';
  oldPassword = '';
  newPassword = '';
  dateCreated = '';

}
