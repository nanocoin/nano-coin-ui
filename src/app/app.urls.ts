
export const Url = {
  logout: 'logout',
  create_user: 'create-user',
  update_user_info: 'update-user-info',
  change_password: 'change-password',

  invitation_link: 'invitation-link',
  send_login_code: 'send-login-code',
  restore_password: 'restore-password',
  get_token: 'get-token',
  get_team: 'get-team',
  get_team_info: 'get-team-info',
  save_avatar: 'save-avatar',
  save_passport_scan: 'save-passport-scan',

  get_news: 'news',

  get_users: 'admin/users',
  toggle_lock_user: 'admin/toggle-user-lock',
  get_all_active_withdrawals: 'admin/get-active-withdrawals',
  news: 'admin/news',
  close_withdrawal: 'admin/close-withdrawal',
  cancel_withdrawal: 'admin/cancel-withdrawal',
  save_admin_settings: 'admin/save-admin-settings',
  get_admin_settings: 'admin/get-admin-settings',

  get_incoming_transaction_log: 'transaction/incoming',
  get_outcoming_transaction_log: 'transaction/outcoming',

  execute_transaction: 'account/transaction',
  update_pin: 'account/pin/update',
  reset_pin: 'account/pin/reset',
  get_usd_account: 'account/usd',
  withdrawal_request: 'account/withdrawal',
  get_active_withdrawals: 'account/withdrawals/active',

  get_all_active_deposits: 'deposit/get-all-active-deposits',
  get_all_closed_deposits: 'deposit/get-all-closed-deposits',
  get_all_waiting_deposits: 'deposit/get-all-waiting-deposits',
  get_all_waiting_for_payment: 'deposit/get-all-waiting-for-payment',
  calculate_income_for_sum: 'deposit/calculate-income-for-sum',
  create_deposit: 'deposit/create-deposit',
  confirm_deposit: 'deposit/confirm-deposit',
  cancel_deposit: 'deposit/cancel-deposit',

  get_available_payment_methods: 'payment/payment-methods',
  get_available_withdraw_payment_methods: 'payment/withdraw-payment-methods'
};
export const ExternalUrl = {
  get_cryptocompare_pairs: 'https://min-api.cryptocompare.com/data/top/pairs?fsym=BTC&limit=20'
};
