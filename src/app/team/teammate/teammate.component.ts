
import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {InvitationService} from '../../invitation/invitation.service';
import {LocalNotificationService} from '../../notification/local.notification.service';

@Component({
  selector: 'app-teammate',
  templateUrl: './teammate.component.html'
})
export class TeamMateComponent {
  loading = false;
  inviteLeftEmail = '';
  inviteRightEmail = '';
  inviteEmail = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private invitationService: InvitationService,
              private localNotificationService: LocalNotificationService,
              public thisDialogRef: MatDialogRef<TeamMateComponent>) {

  }

  onCloseConfirm() {
    this.thisDialogRef.close('Confirm');
  }

}
