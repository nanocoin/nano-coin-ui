import {Component} from '@angular/core';
import {Link, UserNode} from './team-graph/model/';
import {TeamService} from './team.service';
import {TeamInfo} from './model/team-info.model';
import {MatDialog} from '@angular/material';
import {TeamMateComponent} from './teammate/teammate.component';
import {TeamLineBonusLevel} from './model/team-line-bonus-level.model';
import {TeamlineBonusLevelsComponent} from './teamline-bonus-level/teamline-bonus-levels.component';
import {TeamLineBonusRule} from './model/team-line-bonus-rule-model';
import {Invitation} from '../main/model/invitation';
import {InvitationService} from '../invitation/invitation.service';
import {LocalNotificationService} from '../notification/local.notification.service';
import {AuthorizationService} from '../common/authorization.service';
import {Message} from '../common/model/message.model';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html'
})
export class TeamComponent {

  loading = false;

  nodes: UserNode[] = [];
  links: Link[] = [];
  teamInfo = new TeamInfo();
  teamSize = 1;
  amountOfUncontrolledInvitations = 4;
  currentLevel = '';

  graphViewState = true;
  partnersViewState = false;
  infoViewState = false;

  isLeftInvitation = true;

  invitationEmail = '';

  teamMemberSearchUsername = '';

  constructor(private teamService: TeamService,
              private matDialog: MatDialog,
              private authorizationService: AuthorizationService,
              private invitationService: InvitationService,
              private localNotificationService: LocalNotificationService) {
    this.getTeamInfo();
  }

  graphViewAction() {
    this.graphViewState = true;
    this.infoViewState = false;
    this.partnersViewState = false;
  }

  partnersViewAction() {
    this.graphViewState = false;
    this.infoViewState = false;
    this.partnersViewState = true;
  }

  infoViewAction() {
    this.graphViewState = false;
    this.infoViewState = true;
    this.partnersViewState = false;
  }


  leftInvitationSelect() {
    this.isLeftInvitation = true;
  }

  rightInvitationSelect() {
    this.isLeftInvitation = false;
  }

  findTeamMember() {
    let isExists = false;
    this.nodes.forEach(node => {
      if (node._teamMember.username === this.teamMemberSearchUsername) {
        isExists = true;
        this.matDialog.open(TeamMateComponent,
            {data: {node: node}});
      }
    });
    if (!isExists) {
      this.localNotificationService.notifyError('TEAM.NO_SUCH_USERNAME');
    }
  }

  invite() {
    this.loading = true;
    if (this.nodes.length <= this.amountOfUncontrolledInvitations) {
      this.isLeftInvitation = this.nodes.length % 2 === 1 ? true : false;
    }
    const invitation = new Invitation();
    const userInfo = this.authorizationService.getUserDetails()
    invitation.requestEmail = userInfo.email;
    invitation.email = this.invitationEmail;
    invitation.parentEmail = this.isLeftInvitation ? this.getMaxLeftNodeEmail() : this.getMaxRightNodeEmail();
    invitation.left = this.isLeftInvitation;
    this.invitationService.sendInvitationLink(invitation)
        .subscribe(resp => {
          this.loading = false;
          this.localNotificationService.notifyByPrefix('TEAM.INVITATION.', (resp as Message).text
          );
        });
  }

  getMaxLeftNodeEmail() {
    let member = this.nodes[0].teamMember;
    while (member.leftUser != null) {
      member = member.leftUser;
    }
    return member.email;
  }

  getMaxRightNodeEmail() {
    let member = this.nodes[0].teamMember;
    while (member.rightUser != null) {
      member = member.rightUser;
    }
    return member.email;
  }

  showDetails(id) {
    this.nodes.forEach(node => {
      if (node.id === id) {
        this.matDialog.open(TeamMateComponent,
            {data: {node: node}});
      }
    });
  }

  getTeamInfo() {
    this.loading = true;
    this.teamService.getTeamInfo()
        .subscribe(response => {
          this.teamInfo = (response as TeamInfo);
          this.currentLevel = this.getCurrentLevel(this.teamInfo);
          this.loading = false;
        }, error => this.loading = false)
  }

  getCurrentLevel(teamInfo: TeamInfo) {
    let maxRule: TeamLineBonusRule = null;
    teamInfo.teamLineBonusRules.forEach(it => {
      if (+teamInfo.personalDepositTotalSum >= +it.personalDepositSum &&
          +teamInfo.teamTotalTurnover >= +it.totalTurnover) {
        if (maxRule == null || +maxRule.line < +it.line) {
          maxRule = it;
        }
      }
    });
    return maxRule ? maxRule.name : '';
  }
}
