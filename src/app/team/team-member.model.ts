export class TeamMember {
  userId: string;
  email: string;
  parentEmail = '';
  level = '';
  leftUser: TeamMember = null;
  rightUser: TeamMember = null;
  size: number;
  imagePath: string;
  username: string;
  name: string;
  surname: string;
  contactNumber: string;
  contactNumber2: string;
  leftTeamSize: string;
  rightTeamSize: string;

}
