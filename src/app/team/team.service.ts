import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';

import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';
import {AuthorizationService} from '../common/authorization.service';

@Injectable()
export class TeamService {

  constructor(private httpService: HttpService,
              private authorizationService: AuthorizationService) {  }

  getTeam(): Observable<Object> {
    const userInfo = this.authorizationService.getUserDetails()
    return this.httpService.getObservable(Url.get_team + '?userId=' + userInfo.userId + '&email=' + userInfo.email);
  }

  getTeamInfo(): Observable<Object> {
    return this.httpService.getObservable(Url.get_team_info);
  }
}
