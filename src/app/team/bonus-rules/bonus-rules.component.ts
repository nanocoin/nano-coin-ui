
import {Component, Input} from '@angular/core';
import {TeamInfo} from '../model/team-info.model';
import {TeamlineBonusLevelsComponent} from '../teamline-bonus-level/teamline-bonus-levels.component';
import {TeamLineBonusLevel} from '../model/team-line-bonus-level.model';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-team-bonus-rules',
  templateUrl: './bonus-rules.component.html'
})
export class BonusRulesComponent {
  @Input('teamInfo') teamInfo: TeamInfo;

  constructor(private matDialog: MatDialog) {}

  showTeamLineBonusLevels(line: string, teamLineBonusLevels: TeamLineBonusLevel []) {
    this.matDialog.open(TeamlineBonusLevelsComponent,
      {data: {line: line, teamLineBonusLevels: teamLineBonusLevels}});
  }
}
