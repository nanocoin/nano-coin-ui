import { Directive, Input } from '@angular/core';
import { UserNode, TeamGraph } from '../model';

@Directive({
    selector: '[draggableNode]'
})
export class DraggableDirective {
    @Input('draggableNode') draggableNode: UserNode;
    @Input('draggableInGraph') draggableInGraph: TeamGraph;

}
