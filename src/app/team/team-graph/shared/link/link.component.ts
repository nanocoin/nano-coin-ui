import { Component, Input } from '@angular/core';
import { Link } from '../../';

@Component({
  selector: '[link]',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent  {
  @Input('link') link: Link;
}
