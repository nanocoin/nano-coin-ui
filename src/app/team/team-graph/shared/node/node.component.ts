import { Component, Input } from '@angular/core';
import { UserNode } from '../../';
import {TeamComponent} from '../../../team.component';

@Component({
  selector: '[node]',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent {

  @Input('node') node: UserNode;

  @Input('team') team: TeamComponent;

  showDetails(id) {
    this.team.showDetails(id);
  }
}
