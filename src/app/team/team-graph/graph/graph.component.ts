import {Component, Input, ChangeDetectorRef, HostListener, ChangeDetectionStrategy, OnInit} from '@angular/core';
import {D3Service, TeamGraph, UserNode} from '../';
import {TeamService} from '../../team.service';
import {Link} from '../model/link';
import {TeamMember} from '../../team-member.model';
import {TeamComponent} from '../../team.component';

@Component({
  selector: 'graph',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './graph.component.html'
})
export class GraphComponent implements OnInit {

  static Y_MOVEMENT = 300;
  static X_MOVEMENT = 200;

  @Input('nodes') nodes: UserNode [];
  @Input('links') links: Link [];
  @Input('team') team: TeamComponent;

  graph: TeamGraph;
   _options: { width, height } = {width: 600, height: 400};

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.graph.initSimulation(this.options);
  }

  constructor(private d3Service: D3Service, private ref: ChangeDetectorRef, private teamService: TeamService) { }

  ngOnInit() {
    if (this.nodes.length === 0) {
      this.loadTeam();
    }
  }

  loadTeam() {
    this.teamService.getTeam().subscribe(resp => {
      const teamMember = resp as TeamMember;
      this.team.teamSize = teamMember.size;
      this.initGraphElements(teamMember, null, 100, 100, false);
      this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);
      this.graph.ticker.subscribe((d) => {
        this.ref.markForCheck();
      });
    });
  }

  initGraphElements(teamMember: TeamMember, parentTeamMember: TeamMember, x: number, y: number, isLeft: boolean) {
    if (teamMember != null) {
      this.nodes.push(new UserNode(teamMember.userId, x, y, teamMember));
      if (parentTeamMember != null) {
        if (isLeft) {
          this.nodes[this.nodes.length - 2]._left = this.nodes[this.nodes.length - 1];
        } else {
          this.nodes[this.nodes.length - 2]._right = this.nodes[this.nodes.length - 1];
        }
        this.links.push(new Link(parentTeamMember.userId, teamMember.userId));
      }
      if (teamMember.leftUser != null) {
        this.initGraphElements(teamMember.leftUser, teamMember, x - GraphComponent.X_MOVEMENT, y + GraphComponent.Y_MOVEMENT, true);
      }
      if (teamMember.rightUser != null) {
        this.initGraphElements(teamMember.rightUser, teamMember, x + GraphComponent.X_MOVEMENT, y + GraphComponent.Y_MOVEMENT, false);
      }
    }
  }

  get options() {
    return this._options = {
      width: window.innerWidth,
      height: window.innerHeight
    };
  }
}

