
import {TeamMember} from '../../team-member.model';

export class UserNode implements d3.SimulationNodeDatum {
  x: number;
  y: number;
  fx: number | null;
  fy: number | null;

  id: string;
  email: string;
  imagePath: string;
  level: string;

  _left: UserNode = null;
  _right: UserNode = null;

  _teamMember: TeamMember;

  constructor(id, x, y, teamMember: TeamMember) {
    this.id = id;
    this.x = x;
    this.y = y;
    this._teamMember = teamMember;
  }

  get teamMember(): TeamMember {
    return this._teamMember;
  }
  set teamMember(teamMember: TeamMember) {
    this._teamMember = teamMember;
  }

  get left(): UserNode {
    return this._left;
  }
  set left(left: UserNode) {
    this._left = left;
  }

  get right(): UserNode {
    return this._right;
  }
  set right(right: UserNode) {
    this._right = right;
  }
}
