import { UserNode } from './';

export class Link implements d3.SimulationLinkDatum<UserNode> {
  source: UserNode;
  target: UserNode;

  constructor(source, target) {
    this.source = source;
    this.target = target;
  }
}
