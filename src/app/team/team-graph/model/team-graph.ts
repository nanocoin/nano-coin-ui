import { EventEmitter } from '@angular/core';
import { Link } from './link';
import { UserNode } from './node';
import * as d3 from 'd3';

const FORCES = {
  LINKS: 1 / 100
}

export class TeamGraph {
  public ticker: EventEmitter<d3.Simulation<UserNode, Link>> = new EventEmitter();
  public simulation: d3.Simulation<any, any>;

  public nodes: UserNode[] = [];
  public links: Link[] = [];

  constructor(nodes, links, options: { width, height }) {
    this.nodes = nodes;
    this.links = links;
    this.initSimulation(options);
  }


  initNodes() {
    this.simulation.nodes(this.nodes);
  }

  initLinks() {
    this.simulation.force('links',
      d3.forceLink(this.links)
        .id(d => d['id'])
        .strength(FORCES.LINKS)
    );
  }

  initSimulation(options) {
    if (!this.simulation) {
      const ticker = this.ticker;
      this.simulation = d3.forceSimulation();


      this.simulation.on('tick', function () {
        ticker.emit(this);
      });

      this.initNodes();
      this.initLinks();
    }

    this.simulation.force('centers', d3.forceCenter(350, 200));

    this.simulation.restart();
  }
}
