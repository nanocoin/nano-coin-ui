import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-teamline-bonus-levels',
  templateUrl: './teamline-bonus-levels.component.html'
})
export class TeamlineBonusLevelsComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public thisDialogRef: MatDialogRef<TeamlineBonusLevelsComponent>) {

  }
  onCloseConfirm() {
    this.thisDialogRef.close('Confirm');
  }
}
