
import {TeamLineBonusLevel} from './team-line-bonus-level.model';

export class TeamLineBonusRule {
  line: string;
  name: string;
  personalDepositSum: string;
  totalTurnover: string;
  teamLineBonusLevels: TeamLineBonusLevel[];
}
