
import {TeamLineBonusRule} from './team-line-bonus-rule-model';
import {TeamLevelTurnoverPercentageRule} from './team-level-turnover-percentage-rule.model';

export class TeamInfo {
  personalDepositTotalSum: string;
  teamTotalTurnover: string;
  leftTeamTotalTurnover: string;
  rightTeamTotalTurnover: string;
  teamLineBonusRules: TeamLineBonusRule[];
  teamLevelTurnoverPercentageRules: TeamLevelTurnoverPercentageRule[];
}
