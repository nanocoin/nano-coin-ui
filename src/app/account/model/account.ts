
export class Account {
  accountId: string;
  currency: string;
  balance: number;
}
