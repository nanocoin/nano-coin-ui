
import {UserInfo} from '../../main/model/user-info';

export class Withdrawal {
  withdrawalId: string;
  accountId: string;
  sum: number;
  externalAccountType: string;
  externalAccountId: string;
  messageToOperator: string;
  message: string;
  creationDate: string;
  pinCode: string;
  userInfoDTO: UserInfo;
  password: string;
}
