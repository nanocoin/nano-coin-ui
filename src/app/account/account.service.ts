
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';
import {Withdrawal} from './model/withdrawal';
import {Transaction} from './transaction/model/transaction';
@Injectable()
export class AccountService {
  constructor(private httpService: HttpService) {  }

  getUsdAccount(): Observable<Object> {
    return this.httpService.getObservable(Url.get_usd_account);
  }

  getActiveWithdrawals(): Observable<Object> {
    return this.httpService.getObservable(Url.get_active_withdrawals);
  }

  withdraw(withdrawal: Withdrawal): Observable<Object> {
    return this.httpService.postObservable(Url.withdrawal_request, JSON.stringify(withdrawal));
  }

  updatePin(transaction: Transaction): Observable<Object> {
    return this.httpService.postObservable(Url.update_pin, JSON.stringify(transaction));
  }
  resetPin(): Observable<Object> {
    return this.httpService.postObservable(Url.reset_pin, '{}');
  }
  isValidAccountPin(pin: string) {
    const isNum = /^\d+$/.test(pin);
    return isNum && pin.length >= 4 && pin.length <= 8;
  }
}
