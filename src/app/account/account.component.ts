import {Component} from '@angular/core';
import {AccountService} from './account.service';
import {Account} from './model/account';
import {Withdrawal} from './model/withdrawal';
import {Transaction} from './transaction/model/transaction';
import {PaymentService} from '../payment/payment.service';
import {LocalNotificationService} from '../notification/local.notification.service';
import {Message} from '../common/model/message.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent {

  account = new Account();
  oldPin = '';
  confirmOldPin = '';
  newPin = '';
  activeWithdrawals: Withdrawal[] = [];
  paymentMethods: string[] = [];
  showWithdrawalRequests = false;
  showChangePin = false;
  showTransactions = false;

  withdrawal = new Withdrawal();

  constructor(private accountService: AccountService,
              private paymentService: PaymentService,
              private localNotificationService: LocalNotificationService) {
    this.loadAccount();
    this.showTransactionsAction();
  }

  showWithdrawalRequestsAction() {
    this.showWithdrawalRequests = true;
    this.showChangePin = false;
    this.showTransactions = false;
    this.loadActiveWithdrawals();
  }

  showChangePinAction() {
    this.showWithdrawalRequests = false;
    this.showChangePin = true;
    this.showTransactions = false;
  }

  showTransactionsAction() {
    this.showWithdrawalRequests = false;
    this.showChangePin = false;
    this.showTransactions = true;
  }

  loadAccount() {
    this.accountService.getUsdAccount()
        .subscribe(response => this.account = response as Account);
  }

  loadPaymentMethods() {
    this.paymentService.getAvailableWithdrawPaymentMethods()
        .subscribe(response => this.paymentMethods = response as string []);
  }

  loadActiveWithdrawals() {
    this.accountService.getActiveWithdrawals()
      .subscribe(response => this.activeWithdrawals = response as Withdrawal[]);
    this.loadPaymentMethods();
  }

  withdraw() {
    if (this.withdrawal.sum <= 0) {
      this.localNotificationService.notifyError('ACCOUNT.WITHDRAWAL.SUM_SHOULD_BE_POSITIVE');
    } else if (this.withdrawal.sum > this.account.balance) {
      this.localNotificationService.notifyError('ACCOUNT.WITHDRAWAL.NOT_ENOUGH_MONEY');
    } else {
      this.accountService
          .withdraw(this.withdrawal)
          .subscribe(response => {
            this.localNotificationService.notifyByPrefix('ACCOUNT.WITHDRAWAL.', (response as Message).text);
            this.loadActiveWithdrawals();
            this.loadAccount();
        });
    }
    this.withdrawal.pinCode = '';
  }

  updatePin() {
    if (this.oldPin !== this.confirmOldPin) {
      this.localNotificationService.notifyError('ACCOUNT.OLD_PINS_DIFFERENT');
    } else {
      if (!this.accountService.isValidAccountPin(this.newPin) || !this.accountService.isValidAccountPin(this.oldPin)) {
        this.localNotificationService.notifyError('ACCOUNT.PIN_VALIDATION');
      } else {
        const transaction = new Transaction();
        transaction.pinCode = this.oldPin;
        transaction.newPinCode = this.newPin;
        this.accountService.updatePin(transaction)
            .subscribe(response => {
              const message = (response as Message).text;
              if (message === 'WRONG_OLD_PIN') {
                this.localNotificationService.notifyError('ACCOUNT.WRONG_OLD_PIN');
              }
              if (message === 'PIN_CHANGED') {
                this.localNotificationService.notifyCreate('ACCOUNT.PIN_CHANGED');
              }
          });
      }
    }
    this.oldPin = '';
    this.confirmOldPin = '';
    this.newPin = '';
  }

  resetPin() {
    this.accountService.resetPin()
        .subscribe(response => this.localNotificationService.notifyCreate('ACCOUNT.PIN_RESETED'));
  }
}
