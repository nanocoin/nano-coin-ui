import {Component, Input} from '@angular/core';
import {TransactionService} from './transaction.service';
import {Transaction} from './model/transaction';
import {AccountComponent} from '../account.component';
import {LocalNotificationService} from 'app/notification/local.notification.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html'
})
export class TransactionComponent {
  @Input('accountComponent')
  accountComponent: AccountComponent;

  incomingTransactions: Transaction[] = [];
  outcomingTransactions: Transaction[] = [];

  transaction = new Transaction();

  constructor(private transactionService: TransactionService,
              private localNotificationService: LocalNotificationService) {
    this.loadIncomingTransactions();
    this.loadOutcomingTransactions();
  }

  loadIncomingTransactions() {
    this.transactionService.getIncomingTransactionLog()
        .subscribe(response => this.incomingTransactions = response as Transaction[]);
  }

  loadOutcomingTransactions() {
    this.transactionService.getOutomingTransactionLog()
        .subscribe(response => this.outcomingTransactions = response as Transaction[]);
  }

  executeTransaction() {
    if (this.transaction.amount <= 0) {
      this.localNotificationService.notifyError('ACCOUNT.TRANSACTION.WRONG_TRANSACTION_SUM');
    } else if (this.transaction.amount > this.accountComponent.account.balance) {
      this.localNotificationService.notifyError('ACCOUNT.TRANSACTION.BIG_TRANSACTION_SUM');
    } else {
     this.transactionService.executeTransaction(this.transaction)
         .subscribe(response => {
            const responseTransaction = response as Transaction;
            if (responseTransaction.status !== '') {
              this.localNotificationService.notifyByPrefix('ACCOUNT.TRANSACTION.', responseTransaction.status);
            }
            this.transaction.pinCode = '';
            this.loadOutcomingTransactions();
            this.accountComponent.loadAccount();
        });
    }
  }
}
