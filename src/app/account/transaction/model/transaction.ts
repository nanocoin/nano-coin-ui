
export class Transaction {
  transactionId: string;
  from: string;
  to: string;
  amount: number;
  message: string;
  timestamp: string;
  pinCode: string;
  newPinCode: string;
  status: string;
}
