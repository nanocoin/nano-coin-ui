import {Injectable} from '@angular/core';
import {HttpService} from '../../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../../app.urls';

import {Transaction} from './model/transaction';

@Injectable()
export class TransactionService {

  constructor(private httpService: HttpService) {  }

  getIncomingTransactionLog(): Observable<Object> {
    return this.httpService.getObservable(Url.get_incoming_transaction_log);
  }

  getOutomingTransactionLog(): Observable<Object> {
    return this.httpService.getObservable(Url.get_outcoming_transaction_log);
  }

  executeTransaction(transaction: Transaction): Observable<Object> {
    return this.httpService.postObservable(Url.execute_transaction, JSON.stringify(transaction));
  }

}
