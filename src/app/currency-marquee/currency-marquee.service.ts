import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ExternalUrl} from '../app.urls';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CurrencyMarqueeService {

  constructor(private http: HttpClient) { }

  getCurrencies(): Observable<Object> {
    return this.http.get(ExternalUrl.get_cryptocompare_pairs);
  }
}
