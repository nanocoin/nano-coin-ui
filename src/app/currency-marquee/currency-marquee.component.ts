import {CurrencyMarqueeService} from './currency-marquee.service';
import {CurrencyData} from './model/currency-data.model';
import {Component} from '@angular/core';
import {Currencyresp} from './model/currencyresp.model';

@Component({
  selector: 'app-currency-marquee',
  templateUrl: './currency-marquee.component.html',
})
export class CurrencyMarqueeComponent {

  newsList: CurrencyData[];

  constructor(private currencyService: CurrencyMarqueeService) {
    this.loadNews();
  }

  loadNews() {
    this.currencyService.getCurrencies()
        .subscribe(response => this.newsList = (response as Currencyresp).Data)
  }
}
