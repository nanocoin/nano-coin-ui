
import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-image-explorer',
  template: '<img [src]="data.imagePath" alt="">'
})
export class ImageExplorerComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {  }
}
