import {Component, ViewChild, AfterViewInit, ElementRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import SmartySelect from 'smarty-select';
import {CommonUtilsService} from '../common/common-utils-service';

@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html'
})
export class LanguageSelectComponent implements AfterViewInit {
  @ViewChild('initLanguageSelect')
  private initLanguageSelect: ElementRef;

  currentLang = '';

  constructor(public translate: TranslateService) {
    const browserLang = translate.getBrowserLang();
    translate.addLangs(['en', 'ru']);

    if (CommonUtilsService.getLang() == null) {
      CommonUtilsService.setLang(browserLang.match(/en|ru/) ? browserLang : 'en');
    }

    this.currentLang = CommonUtilsService.getLang();
    translate.use(CommonUtilsService.getLang());
  }

  ngAfterViewInit() {
    const select = SmartySelect(this.initLanguageSelect.nativeElement);

    select.change((el) => {
      const currentValue = el.getAttribute('data-value');
      this.translate.use(currentValue);
      CommonUtilsService.setLang(currentValue);
    });
  }
}
