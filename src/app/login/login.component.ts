import {Component} from '@angular/core';
import {trigger, state, style, transition, animate, keyframes} from '@angular/animations';
import {LoginService} from './login.service';
import {Validators, FormBuilder} from '@angular/forms';
import {AuthorizationService} from '../common/authorization.service';
import {LocalNotificationService} from '../notification/local.notification.service';
import {Error400} from '../common/model/error.model';
import {Message} from 'app/common/model/message.model';
import {Token} from '../common/model/token.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  animations: [
    trigger('showPinField', [
      state('hide', style({
        display: 'none',
        height: '0px',
        opacity: '0',
      })),
      transition('hide => show', animate('500ms ease-in', keyframes([
        style({
          display: 'none',
          height: '0px',
          opacity: '0',
        }),
        style({
          display: 'block',
          height: '*',
        }),
        style({
          opacity: '1',
        }),
      ]))),
    ]),

    trigger('showButtons', [
      state('hide', style({
        display: 'none',
        width: '0px',
        opacity: '0',
      })),
      transition('hide => show', animate('500ms ease-in', keyframes([
        style({
          display: 'none',
          width: '0px',
          opacity: '0',
        }),
        style({
          display: 'block',
          width: '*',
        }),
        style({
          opacity: '1',
        }),
      ]))),
    ]),

    trigger('openRegistrationForm', [
      state('hide', style({
        display: 'none',
        transform: 'translate(-100%, 0)',
      })),
      state('show', style({
        display: 'block',
        transform: 'translate(0)',
      })),
      transition('hide => show', animate('300ms ease-in')),
    ]),
  ],
})
export class LoginComponent {

  public registrationState = 'hide';
  public askedForPin = 'hide';
  public wrongEmailPassword = 'hide';
  public loading = false;

  public loginForm = this.fb.group({
    email: ['', Validators.email],
    password: ['', Validators.required],
    pinCode: ['']
  });

  constructor(private loginService: LoginService,
              private authGuardService: AuthorizationService,
              private fb: FormBuilder,
              private localNotificationService: LocalNotificationService,
              private authorizationService: AuthorizationService) { }

  sendCode() {
    this.authorizationService.logout();
    this.loading = true;
    this.loginService
      .sendLoginCode(this.loginForm.value)
      .subscribe(response => {
        if (response instanceof Error400) {
          response.errors.forEach(error => {
            this.localNotificationService.notifyError('LOGIN.FORM.' + error);
          })
        } else {
          const message = response as Message;
          if (message.text === 'ENTER_PIN') {
            this.askedForPin = 'show';
          } else if (message.text === 'WRONG_EMAIL_PASSWORD') {
            this.wrongEmailPassword = 'show';
          }
          this.localNotificationService.notifyByPrefix('LOGIN.FORM.', message.text);
        }
        this.loading = false;
      });
  }

  login() {
    if (this.askedForPin === 'hide') {
      this.sendCode();
    } else {
      this.loading = true;
      this.loginService
          .login(JSON.stringify(this.loginForm.value))
          .subscribe(response  => {
            const token = response as Token;
            if (token.message !== '') {
              this.localNotificationService.notifyByPrefix('LOGIN.FORM.', token.message);
            } else {
              this.authGuardService.login(token.storageToken);
            }
            this.loading = false;
          }, error => {
            this.localNotificationService.notifyByPrefix('', error.message);
            this.loading = false;
          });
    }
  }

  restorePassword() {
    this.loginService
        .restorePassword(this.loginForm.value)
        .subscribe(response => this.localNotificationService.notifyByPrefix('LOGIN.FORM.', (response as Message).text));
  }

  openRegistrationForm() {
    this.registrationState = 'show';
  }
}
