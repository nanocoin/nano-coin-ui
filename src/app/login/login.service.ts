
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';

@Injectable()
export class LoginService {

  constructor(private httpService: HttpService) {  }

  login(loginInfoJson: string): Observable<Object> {
    return this.httpService.postObservable(Url.get_token, loginInfoJson);
  }

  sendLoginCode(loginJson: JSON): Observable<Object> {
    return this.httpService.postObservable(Url.send_login_code, JSON.stringify(loginJson));
  }

  restorePassword(loginJson: JSON): Observable<Object> {
    return this.httpService.postObservable(Url.restore_password, JSON.stringify(loginJson));
  }

}
