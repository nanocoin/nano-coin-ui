import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {LoginService} from './login/login.service';
import {HttpService} from './common/http-service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';
import {AuthorizationService} from './common/authorization.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {RegistrationService} from './registration/registration.service';
import {RegistrationComponent} from './registration/registration.component';
import {LoadingModule} from 'ngx-loading';
import {appRoutes} from './app.routes';
import {TeamComponent} from './team/team.component';
import {TeamService} from './team/team.service';

import {D3Service, D3_DIRECTIVES} from './team/team-graph';
import {GraphComponent} from './team/team-graph/graph/graph.component';
import {SHARED_VISUALS} from './team/team-graph/shared';
import {TransactionComponent} from 'app/account/transaction/transaction.component';
import {TransactionService} from 'app/account/transaction/transaction.service';
import {DepositComponent} from 'app/deposit/deposit.component';
import {DepositService} from './deposit/deposit.service';
import {PaymentService} from 'app/payment/payment.service';
import {AdminComponent} from 'app/admin/admin.component';
import {AdminService} from './admin/admin.service';
import {InvitationService} from './invitation/invitation.service';
import {UserDetailsService} from './user-details/user-details.service';
import {UserDetailsComponent} from './user-details/user-details.component';
import {AccountComponent} from './account/account.component';
import {AccountService} from './account/account.service';
import {HeaderComponent} from './header/header.component';
import {LanguageSelectComponent} from './language-select/language-select.component';
import {NewsComponent} from './news/news.component';
import {NewsService} from './news/news.service';
import {AboutCompanyComponent} from './about-company/about-company.component';
import {ContactsComponent} from './contacts/contacts';
import {MyDatePickerModule} from 'mydatepicker';
import {Ng2ImgMaxModule} from 'ng2-img-max';
import {FooterComponent} from './footer/footer.component';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {LocalNotificationService} from './notification/local.notification.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {CountryService} from 'app/country/country.service';
import {MatDialogModule, MatSliderModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select'
import {TeamMateComponent} from './team/teammate/teammate.component';
import {TeamlineBonusLevelsComponent} from './team/teamline-bonus-level/teamline-bonus-levels.component';
import {ImageExplorerComponent} from './image-explorer/image-explorer.component';
import {SanitizeHtmlPipe} from 'app/pipe/sanitize-html-pipe';
import {ParticlesModule} from 'angular-particle';
import { CurrencyMarqueeComponent } from './currency-marquee/currency-marquee.component';
import {CurrencyMarqueeService} from './currency-marquee/currency-marquee.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientModule} from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import 'hammerjs';
import {ChangePasswordComponent} from './user-details/change-password/change-password.component';
import {ChangeAvatarComponent} from "app/user-details/change-avatar/change-avatar.component";
import {AccountSettingsComponent} from "app/user-details/account-settings/account-settings.component";
import {CreateDepositComponent} from "app/deposit/create-deposit/create-deposit.component";
import {BonusRulesComponent} from "app/team/bonus-rules/bonus-rules.component";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    TeamMateComponent,
    LoginComponent,
    RegistrationComponent,
    AdminComponent,
    TeamComponent,
    GraphComponent,
    BonusRulesComponent,
    ...SHARED_VISUALS,
    ...D3_DIRECTIVES,
    AccountComponent,
    TransactionComponent,
    DepositComponent,
    CreateDepositComponent,
    UserDetailsComponent,
    AccountSettingsComponent,
    ChangePasswordComponent,
    ChangeAvatarComponent,
    HeaderComponent,
    LanguageSelectComponent,
    NewsComponent,
    AboutCompanyComponent,
    ContactsComponent,
    FooterComponent,
    TeamlineBonusLevelsComponent,
    ImageExplorerComponent,
    SanitizeHtmlPipe,
    CurrencyMarqueeComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    LoadingModule,
    MyDatePickerModule,
    Ng2ImgMaxModule,
    NgxPaginationModule,
    MatDialogModule,
    MatSelectModule,
    ParticlesModule,
    MatSliderModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [HttpService,
    AuthorizationService,
    LoginService,
    RegistrationService,
    AdminService,
    TeamService,
    D3Service,
    AccountService,
    TransactionService,
    DepositService,
    PaymentService,
    InvitationService,
    UserDetailsService,
    LocalNotificationService,
    NewsService,
    CurrencyMarqueeService,
    CountryService],
  bootstrap: [AppComponent],
  entryComponents: [TeamMateComponent, TeamlineBonusLevelsComponent, ImageExplorerComponent],
  exports: [SanitizeHtmlPipe]
})
export class AppModule {
}
