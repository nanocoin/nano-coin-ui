
import {Injectable} from '@angular/core';
import {HttpService} from '../common/http-service';
import {Observable} from 'rxjs/Observable';
import {Url} from '../app.urls';

@Injectable()
export class NewsService {
  constructor(private httpService: HttpService) {  }

  getNews(): Observable<Object> {
    return this.httpService.getObservable(Url.get_news);
  }
}
