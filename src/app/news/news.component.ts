import {Component} from '@angular/core'
import {News} from './model/news.model';
import {NewsService} from './news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html'
})
export class NewsComponent {

  pageNumber: number;

  newsList: News[] = [];

  constructor(private newsService: NewsService) {
    this.loadNews();
  }

  loadNews() {
    this.newsService.getNews()
        .subscribe(response => this.newsList = (response as News[]));
  }
}
