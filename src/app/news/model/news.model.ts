
export class News {
  id: string;
  subject = '';
  message = '';
  dateCreated: string;
  error: string;
}
