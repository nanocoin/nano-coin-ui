import {Injectable} from '@angular/core';
import 'rxjs/add/operator/catch';
import {AuthorizationService} from 'app/common/authorization.service';
import {Url} from '../app.urls';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {Error400} from 'app/common/model/error.model';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Injectable()
export class HttpService {

  public static readonly baseApiPath = 'https://nano-mlm-core.herokuapp.com';

  constructor(private http: HttpClient,
              private authorizationService: AuthorizationService) {
  }

  public getObservable(url: string): Observable<Object> {
    return this.http.get(`${HttpService.baseApiPath}/${url}`, this.getHeaders())
      .catch((error) => Observable.of(this.handleError(error)));
  }

  public postObservable(url: string, body: string): Observable<Object> {
    return this.http.post(`${HttpService.baseApiPath}/${url}`, body, this.getHeaders())
      .catch((error) => Observable.of(this.handleError(error)));
  }

  public deleteObservable(url: string, id: string): Observable<Object> {
    return this.http.delete(`${HttpService.baseApiPath}/${url}/${id}`, this.getHeaders())
      .catch((error) => Observable.of(this.handleError(error)));
  }

  public postImage(url: string, formData: FormData): Observable<Object> {
    return this.http.post(`${HttpService.baseApiPath}/${url}`, formData, this.getHeadersForImage())
                    .catch(error => Observable.of(this.handleError(error)));
  }

  private getHeadersForImage() {
    return {
      headers: new HttpHeaders(
        {
          'Authorization': localStorage.getItem(AuthorizationService.authTokenKey),
          'Accept': 'application/json',
        }
      )
    }
  }

  private getHeaders() {
    return {
      headers: new HttpHeaders(
        {
          'Authorization': localStorage.getItem(AuthorizationService.authTokenKey),
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      )
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status !== 400) {
      this.authorizationService.logout();
    }
    const errorMessages = error.error.errors.map(it => it.defaultMessage);
    return Error400.toError(errorMessages);
  }

  logout() {
    this.getObservable(Url.logout)
      .subscribe(response => {

      });
    this.authorizationService.logout();
  }
}
