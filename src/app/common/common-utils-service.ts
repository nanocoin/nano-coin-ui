
export class CommonUtilsService {
  static getIdByEvent(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    return idAttr.value;
  }
  static getContent(event): string {
    const target = event.target || event.srcElement || event.currentTarget;
    return target.innerHTML;
  }
  static setContent(event, content) {
    const target = event.target || event.srcElement || event.currentTarget;
    target.innerHTML = content;
  }

  static setLang(lang: string) {
    return localStorage.setItem('lang', lang);
  }
  static getLang() {
    return localStorage.getItem('lang');
  }
}
