import {Injectable} from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {JwtHelper} from 'angular2-jwt';
import {UserInfo} from '../main/model/user-info';

@Injectable()
export class AuthorizationService implements CanActivate {

  public static readonly authTokenKey = 'token';

  public static readonly adminPath = 'admin';

  constructor(private router: Router) {}

  login(token: string) {
    localStorage.setItem(AuthorizationService.authTokenKey, token);
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.setItem(AuthorizationService.authTokenKey, '')
    this.router.navigate(['/login']);
  }

  getToken() {
    return localStorage.getItem(AuthorizationService.authTokenKey);
  }

  isLoggedIn() {
    const token = this.getToken();
    return token != null && token !== '';
  }

  getUserDetails() {
    const token = new JwtHelper().decodeToken(this.getToken());
    const sub = JSON.parse(token['sub']);
    const userInfo = new UserInfo();
    userInfo.userId = sub['id'];
    userInfo.email = sub['email'];
    userInfo.username = sub['username'];
    userInfo.name = sub['name'];
    userInfo.surname = sub['surname'];
    userInfo.contactNumber = sub['contactNumber'];
    userInfo.contactNumber2 = sub['contactNumber2'];
    userInfo.referralId = sub['referralId'];
    userInfo.address = sub['address'];
    userInfo.city = sub['city'];
    userInfo.country = sub['country'];
    userInfo.birthdayDate = sub['birthdayDate'];
    userInfo.man = sub['man'];
    userInfo.passportId = sub['passportId'];
    userInfo.parentEmail = sub['parentEmail'];
    userInfo.roleName = sub['role'];
    return userInfo;
  }

  getRole() {
    return this.getUserDetails().roleName;
  }

  isAdmin() {
    return this.getRole() === 'admin';
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.isLoggedIn()) {
      if (route.url.toString() === AuthorizationService.adminPath) {
        if (this.isAdmin()) {
          return true;
        }
        this.router.navigate(['/']);
        return false;
      }
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

}
