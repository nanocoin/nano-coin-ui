
export class Error400 {
  errors: string [] = [];

  static toError(errors: string []) {
    const error = new Error400();
    error.errors = errors;
    return error;
  }
}
