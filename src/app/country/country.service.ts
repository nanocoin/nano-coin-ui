import {Injectable} from '@angular/core';
import {Country} from './model/country-model';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CountryService {

  countries: Country [] = [];

  constructor(private http: HttpClient) {
    this.loadCountries();
  }

  loadCountries() {
    this.http.get('assets/countries.json')
        .subscribe(response => this.countries = response as Country[]);
  }

}
