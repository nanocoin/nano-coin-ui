import { NanoCoinUiPage } from './app.po';

describe('nano-coin-ui App', () => {
  let page: NanoCoinUiPage;

  beforeEach(() => {
    page = new NanoCoinUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
